<?php

namespace Drupal\janrain_connect_settings\Constants;

/**
 * JanrainConnectSettingsConstants.
 */
class JanrainConnectSettingsConstants {

  /**
   * Setting password recover url.
   */
  const JANRAIN_CONNECT_SETTINGS_PASSWORD_RECOVER_URL = 'password_recover_url';

}
