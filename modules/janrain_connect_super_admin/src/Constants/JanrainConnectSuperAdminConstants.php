<?php

namespace Drupal\janrain_connect_super_admin\Constants;

/**
 * Janrain Connect Super Admin Constants.
 */
class JanrainConnectSuperAdminConstants {

  /**
   * Role ID for administrator users.
   */
  const ADMINISTRATOR_ROLE = 'administrator';

  /**
  * Message Error.
  */
  const MSG_ERROR = 'Service Unavailable';

}
