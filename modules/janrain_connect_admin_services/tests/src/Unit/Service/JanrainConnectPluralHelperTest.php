<?php

namespace Drupal\Tests\janrain_connect_admin_services\Unit\Service;

use Drupal\janrain_connect_admin_services\Service\JanrainConnectPluralHelper;
use Drupal\janrain_connect_admin_services\Service\JanrainConnectAdminServicesCalls;
use Drupal\Tests\UnitTestCase;

/**
 * Class JanrainConnectPluralHelperTest.
 *
 * @group janrain_connect
 * @coversDefaultClass \Drupal\janrain_connect_admin_services\Service\JanrainConnectPluralHelper
 */
class JanrainConnectPluralHelperTest extends UnitTestCase {

  /**
   * Fake user object.
   *
   * @var \stdClass
   */
  private $janrainUserFake;

  /**
   * Fake data return.
   *
   * @var array
   */
  private $returnData;

  /**
   * Plural helper mock.
   *
   * @var \Drupal\janrain_connect_admin_services\Service\JanrainConnectPluralHelper
   */
  private $janrainPluralHelper;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    // TODO: Change the autogenerated stub.
    parent::setUp();
    // Fake user.
    $this->janrainUserFake = new \stdClass();

    $this->returnData = [
      'has_errors' => FALSE,
      'result_count' => 1,
      'results' => [
        $this->janrainUserFake,
      ],
    ];

    $this->janrainAdminServiceCall = $this
      ->getMockBuilder(JanrainConnectAdminServicesCalls::class)
      ->disableOriginalConstructor()
      ->setMethods(['findUser'])
      ->getMock();

    $this->janrainAdminServiceCall
      ->method('findUser')
      ->with('email=????')
      ->willReturn($this->returnData);

    $this->janrainPluralHelper = new JanrainConnectPluralHelper($this->janrainAdminServiceCall);
  }

  /**
   * @covers ::getPlural
   */
  public function testGetPlural() {
    // Fake plural.
    $plural_fake = new \stdClass();
    $plural_fake->id = 0;
    $plural_fake->granted = FALSE;
    $plural_fake->name = 'id_marketing';
    $plural_fake->updated = '2019-03-20 13:25:01 +0000';

    $this->janrainUserFake->plural_name = [$plural_fake];

    // Assert search for invalid plural name.
    $this->assertFALSE($this->janrainPluralHelper->getPlural('unavailable_plural', '????'));

    // Array return.
    $expected_plural = [
      0 => [
        'id' => 0,
        'granted' => FALSE,
        'name' => 'id_marketing',
        'updated' => '2019-03-20 13:25:01 +0000',
      ],
    ];

    // Assert search for plural name matching the existent one.
    $this->assertSame($expected_plural, $this->janrainPluralHelper->getPlural('plural_name', '????', ['name' => 'id_marketing']));

    // Assert search for plural name not matching the existent one.
    $this->assertSame($expected_plural, $this->janrainPluralHelper->getPlural('plural_name', '????', ['name' => 'id_marketing_xx']));

    // Fake plural.
    $plural_fake_2 = new \stdClass();
    $plural_fake_2->id = 1;
    $plural_fake_2->granted = FALSE;
    $plural_fake_2->name_xx = 'id_marketing_xx';
    $plural_fake_2->updated = '2019-03-20 13:25:01 +0000';

    $this->janrainUserFake->plural_name = [$plural_fake, $plural_fake_2];

    // Array return.
    $expected_plural = [
      0 => [
        'id' => 0,
        'granted' => FALSE,
        'name' => 'id_marketing',
        'updated' => '2019-03-20 13:25:01 +0000',
      ],
      1 => [
        'id' => 1,
        'granted' => FALSE,
        'name_xx' => 'id_marketing_xx',
        'updated' => '2019-03-20 13:25:01 +0000',
      ],
    ];

    $plural = [
      'name_xx' => 'id_marketing_xx',
      'name'    => 'id_marketing',
    ];

    $this->assertSame($expected_plural, $this->janrainPluralHelper->getPlural('plural_name', '????', $plural));
  }

}
