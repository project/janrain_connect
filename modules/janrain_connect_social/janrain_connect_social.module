<?php

/**
 * @file
 * Main file for the Janrain Connect Block.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\janrain_connect_social\Constants\JanrainConnectSocialConstants;
use JanrainRest\JanrainRest as Janrain;

/**
 * Implements hook_theme().
 */
function janrain_connect_social_theme($existing, $type, $theme, $path) {
  return [
    'janrain_connect_social' => [
      'variables' => [
        'providers' => [],
      ],
      'path' => drupal_get_path('module', 'janrain_connect_social') . '/templates',
      'template' => 'component--social_login',
    ],
  ];
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @Todo: Check ID for login form (as block).
 */
function janrain_connect_social_form_janrain_connect_form_signinform_alter(&$form, FormStateInterface $form_state, $form_id) {
  $config = \Drupal::config('janrain_connect.settings');

  if (!$config->get('enable_social_login')) {
    return;
  }

  $janrainApi = new Janrain(
    $config->get('capture_server_url'),
    $config->get('config_server'),
    $config->get('flowjs_url'),
    '',
    '',
    $config->get('client_id'),
    $config->get('client_secret'),
    $config->get('application_id'),
    $config->get('default_language'),
    $config->get('flow_name'),
    \Drupal::logger('janrain_connect_social')
  );

  $social_links = [];
  $providers = $janrainApi->providers();
  $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
  // TODO: Improve User Journey.
  // Need to store which URL will print our social Registration form.
  $token_url = 'http://drupal8.dev.com/janrain/form/socialRegistrationForm';

  foreach ($providers['providers'] as $provider) {
    $social_links[] = [
      'name' => $provider,
      'logo' => sprintf(JanrainConnectSocialConstants::JANRAIN_CONNECT_SOCIAL_LOGO, $provider),
      'link' => $janrainApi->socialLoginUrl($provider, $token_url, $language),
    ];
  }

  $socialFormElement['janrain_connect_social'] = [
    '#theme' => 'janrain_connect_social',
    '#providers' => $social_links,
  ];

  // Puts Social Element as first element on our form.
  $form = array_merge($socialFormElement, $form);
}
