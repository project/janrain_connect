<?php

namespace Drupal\janrain_connect_social\Constants;

/**
 * Janrain Connect Social Constantes.
 */
class JanrainConnectSocialConstants {

  /**
   * Code error for new account.
   */
  const JANRAIN_CONNECT_SOCIAL_NEW_ACCOUNT_CODE = '310';

  /**
   * Code error for merge account.
   */
  const JANRAIN_CONNECT_SOCIAL_EXISTING_ACCOUNT_CODE = '380';

  /**
   * Janrain traditional login name.
   */
  const JANRAIN_CONNECT_SOCIAL_TRADITIONAL_NAME = 'capture';

  /**
   * Social media logo path.
   */
  const JANRAIN_CONNECT_SOCIAL_LOGO = 'https://d3hmp0045zy3cs.cloudfront.net/HEAD/icons/janrain-providers/24/%s.png';

}
