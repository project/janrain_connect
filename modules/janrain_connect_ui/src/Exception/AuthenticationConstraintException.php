<?php

namespace Drupal\janrain_connect_ui\Exception;

use Exception;

/**
 * AuthenticationConstraintException class.
 */
class AuthenticationConstraintException extends Exception {

}
