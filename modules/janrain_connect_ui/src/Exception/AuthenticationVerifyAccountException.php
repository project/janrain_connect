<?php

namespace Drupal\janrain_connect_ui\Exception;

use Exception;

/**
 * AuthenticationVerifyAccountException class.
 */
class AuthenticationVerifyAccountException extends Exception {

}
