<?php

namespace Drupal\janrain_connect_ui\Exception;

use Exception;

/**
 * AuthenticationDrupalLoginException class.
 */
class AuthenticationDrupalLoginException extends Exception {

}
