<?php

namespace Drupal\janrain_connect_ui\Event;

/**
 * Defines events for the Janrain Connect project.
 */
final class JanrainConnectUiEvents {

  /**
   * Name of the event.
   */
  const EVENT_SUBMIT = 'janrain.event_submit';

  /**
   * Name of the event.
   */
  const EVENT_ALTER = 'janrain.event_alter';

}
