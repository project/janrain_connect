<?php

namespace Drupal\janrain_connect\Constants;

/**
 * Janrain Constants.
 */
class JanrainConnectConstants {

  /**
   * Content of the success registration message.
   */
  const JANRAIN_CONNECT_REGISTRATION_MESSAGE_SUCCESS = 'Created a new record';

  /**
   * Content of the generic error for registration.
   */
  const JANRAIN_CONNECT_REGISTRATION_GENERIC_ERROR = 'Error on Register';


  /**
   * Content of the generic error for authentication.
   */
  const JANRAIN_CONNECT_AUTHENTICATION_GENERIC_ERROR = 'Error on Authentication';

  /**
   * Content of the generic error for edit profile.
   */
  const JANRAIN_CONNECT_EDIT_PROFILE_GENERIC_ERROR = 'Error on Edit Profile';

  /**
   * Content of the generic error for forgot password.
   */
  const JANRAIN_CONNECT_FORGOT_PASSWORD_GENERIC_ERROR = 'Error on Forgot Password';

  /**
   * Content of the verify account error for authentication.
   */
  const JANRAIN_CONNECT_AUTHENTICATION_VERIFY_ACCOUNT_ERROR = 'Please check your email to verify your account before you can log in.';

  /**
   * Content of the generic error for changing password.
   */
  const JANRAIN_CONNECT_CHANGE_PASSWORD_GENERIC_ERROR = 'Error changing password';

  /**
   * Content of the generic error for changing password no auth.
   */
  const JANRAIN_CONNECT_CHANGE_PASSWORD_NO_AUTH_GENERIC_ERROR = 'Error changing password';

  /**
   * Content of the message 'Unknown error'.
   */
  const JANRAIN_CONNECT_UNKNOWN_ERROR = 'Unknown error';

  /**
   * Content of the success registration message.
   */
  const JANRAIN_CONNECT_CHANGE_PASSWORD_MESSAGE_SUCCESS = 'Password Updated';

  /**
   * Content of the success change password no auth message.
   */
  const JANRAIN_CONNECT_CHANGE_PASSWORD_NO_AUTH_MESSAGE_SUCCESS = 'Password Updated';

  /**
   * Content of the success of Verification User message.
   */
  const JANRAIN_CONNECT_RESEND_VERIFICATION_USER_SUCCESS = 'E-mail Sended';

  /**
   * Content of the welcome message.
   */
  const JANRAIN_CONNECT_REGISTRATION_MESSAGE_WELCOME = 'Welcome';

  /**
   * Content of the success forgot password message.
   */
  const JANRAIN_CONNECT_FORGOT_PASSWORD_MESSAGE_SUCCESS = "We've sent an email with instructions to create a new password. Your existing password has not been changed.";

  /**
   * Content of the success profile update message.
   */
  const JANRAIN_CONNECT_PROFILE_UPDATE_MESSAGE_SUCCESS = 'Profile successfully updated';

  /**
   * Content of the success login message.
   */
  const JANRAIN_CONNECT_LOGIN_MESSAGE_SUCCESS = 'You are successfully logged in';

}
